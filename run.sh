#!/bin/bash

./stop.sh
docker network create --gateway 172.20.0.1 --subnet 172.20.0.0/16  demo-net
docker volume create demo-data
docker run -p 3333:3333 -d --network demo-net --name demo-article -v demo-data:/tmp/demo-data wuuemm/demo-article
docker run -p 5555:5555 -d --network demo-net --name demo-order -v demo-data:/tmp/demo-data wuuemm/demo-order
docker run -p 80:80 -d --name demo-front  wuuemm/demo-front
