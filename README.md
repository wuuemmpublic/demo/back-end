# Demo project
## Architecture

* Back-end
  * Article Microservice
  * Order MicroService
  * Datastore
* Front-end
  * Web App

#### The whole stuff runs in _Docker_ environment, so _docker_ must be installed locally.

### Communication

A _docker_ network is established on each run to allow for communication between microservices.
So far, in this demo, internal _API http calls_ are used but in a production environment other techniques
should be applied such as:
* message broker
* websockets

### Article Microservice
Internal service handling articles (code, name, availability in stock) and reservations made
(when an order is placed).

This service is __not intended__ to be available from front-end dedicated for
customers. Should be exclusively used by a front-end app used for administration
(by the staff). This app doesn't exist in this demo, though.

API calls to this service are made internally by _Order Microservice_.
Those calls rely on basic http authorization.

The service runs within docker internal network at http://172.20.0.1:3333, the container exposes its _3333_
port to the docker network, thus to the host as well. 

### Order Microservice

This service exposes its API to front-end, simulates placing orders
(reservations) and completing them. Communicates internally with the
_Article Microservice_, as list of articles, stock size per article
and reservations made are this service's responsibility.

The service runs within docker internal network at http://172.20.0.1:5555, the container exposes its _5555_
port to the docker network, thus to the host as well.

Access-Control policy allows for calls from front-end address http://127.0.0.1:80 exclusively.

### Datastore

Data are persisted in docker's _volume_ created on the first run and not deleted unless manually removed.

**H2 Database** in _file mode_ is used for data storage. Each microservice processes its exclusive set
of data tables, to assure loose coupling.

### Front end (see code [here](https://gitlab.com/wuuemmpublic/demo/front-end-demo))

A simple Angular app, using Bootstrap, but not excessively styled. It uses _node.js_ and _nginx_ server images.
On each run on a _new_ container the application is built and deployed to the server. Available at http://127.0.0.1:80

## How to launch

Two scripts are available in the root folder:
* **run.sh**
  * launches _stop.sh_ (just in case a user did not)
  * pulls necessary images from the Docker Hub in necessary
  * sets up a docker internal network
  * creates a docker volume for data storage if not already exists
  * creates and runs three containers based on existing locally or freshly pulled images
* **stop.sh**
  * stops containers
  * removes them to avoid name conflict
  * removes the internal network

Containers start in the _detached_ mode.

Once containers are launched, ___the front-end app may be used in a browser locally at http://127.0.0.1:80___

## Further information

Back-end's structure available in [Javadoc](./javadoc/index.html), however only APIs (back-end) controllers
are so far commented.

Don't hesitate to ask question: [office@profacilis.com](mailto:office@profacilis.com)
