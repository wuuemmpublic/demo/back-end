package com.profacilis.demo.order;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.profacilis.demo.order.APITools.*;

/**
 * API accessible and dedicated for front-end
 * Responsible for articles' list check, make reservations and purchase
 */

@RestController
public class OrderController {

	private final OrderService orderService;

	public OrderController(OrderService orderService) {
		this.orderService = orderService;
	}

	/**
	 * reserves an amount of articles of one kind (if available)
	 * @param articleId article's code
	 * @param amount amount reserved
	 * @return order placed
	 */
	@GetMapping("/{article}/{amount}/place")
	public Order place(@PathVariable("article") String articleId, @PathVariable("amount") int amount) {
		return response(() -> orderService.place(articleId, amount));
	}

	/**
	 * clears all data in both services (Order, Article)
	 * purges all orders and reservations and restores articles' stock to the initial state
	 * @return list of orders, which should be empty after successful operation
	 */
	@GetMapping("/clear")
	public List<Order> clear() {
		return response(orderService::clear);
	}

	/**
	 * @return list of articles in stock
	 */
	@GetMapping("/articles")
	public List<ArticleDTO> articles() {
		return response(orderService::articles);
	}

	/***
	 * @return list of all orders (regardless on status)
	 */
	@GetMapping("/")
	public List<Order> all() {
		return response(orderService::orders);
	}

	/**
	 * finalizes a previously placed order (if not yet completed and exists)
	 * @param orderId order's code
	 * @return processed order
	 */
	@GetMapping("/{order}/buy")
	public Order buy(@PathVariable("order") String orderId) {
		return response(() -> orderService.buy(orderId));
	}

}
