package com.profacilis.demo.order;

public enum OrderStatus {
	RESERVED, BOUGHT
}
