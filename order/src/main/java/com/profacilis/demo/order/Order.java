package com.profacilis.demo.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private Long id;

	@Enumerated(EnumType.STRING)
	private OrderStatus status;

	private String articleId;
	private String code;
	private int amount;

	public Order(String articleId, int amount) {
		this.articleId = articleId;
		this.amount = amount;
		code = UUID.randomUUID().toString().replace("-","").toUpperCase();
		status = OrderStatus.RESERVED;
	}
}
