package com.profacilis.demo.order;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
class ArticleDTO {

	private Long id;
	private String name;
	private int stock;
	private String code;
	private int available;

}
