package com.profacilis.demo.order;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.Base64;
import java.util.List;

@Slf4j
class APITools {

	private final static RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();

	interface ServiceThrowable<T> {
		T get() throws Exception;
	}

	interface ServiceVoidThrowable {
		void get() throws Exception;
	}

	interface ManagesRollback {
		void invalidate();
	}

	static class DemoException extends Exception {
		public DemoException(String message) {
			super(message);
		}
	}

	static <T> T response(ServiceThrowable<T> action) {
		try {
			return action.get();
		} catch (DemoException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	static void responseVoid(ServiceVoidThrowable action) {
		try {
			action.get();
		} catch (DemoException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	static <S> List<ArticleDTO> send(String url, ManagesRollback rollBack) throws DemoException {
		return send(url, null, rollBack);
	}

	static <S> List<ArticleDTO> send(String url, S object) throws DemoException {
		return send(url, object, null);
	}

	static <S> List<ArticleDTO> send(String url) throws DemoException {
		return send(url, null, null);
	}

	public static <S> List<ArticleDTO> send(String url, S object, ManagesRollback rollBack) throws DemoException {
		HttpEntity<S> entity;
		HttpMethod method;
		if (object == null) {
			entity = new HttpEntity<>(buildHeaders());
			method = HttpMethod.GET;
		} else {
			entity = new HttpEntity<>(object, buildHeaders());
			method = HttpMethod.POST;
		}
		try {
			return buildTemplate().exchange(url, method, entity, new ParameterizedTypeReference<List<ArticleDTO>>() {}).getBody();
		} catch (HttpClientErrorException e) {
			if (rollBack != null) rollBack.invalidate();
			throw new DemoException(e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			if (rollBack != null) rollBack.invalidate();
			throw new DemoException(e.getMessage());
		}
	}

	private static MultiValueMap<String, String> buildHeaders() {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		String srcToken = "demo:secret";
		String token = Base64.getEncoder().encodeToString(srcToken.getBytes());
		headers.add(HttpHeaders.AUTHORIZATION, "Basic " + token);
		return headers;
	}

	private static RestTemplate buildTemplate() {
		return restTemplateBuilder.build();
	}

}
