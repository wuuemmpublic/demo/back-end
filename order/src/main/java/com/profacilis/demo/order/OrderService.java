package com.profacilis.demo.order;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service layer of Order microservice
 */

@Service
@Slf4j
public class OrderService {

	public static final String ARTICLE_SERVER = "http://172.20.0.1:3333/";

	private final OrderRepository orderRepository;

	public OrderService(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	public List<Order> orders() {
		return orderRepository.findAll();
	}

	public Order place(String articleId, int amount) throws APITools.DemoException {
		List<ArticleDTO> article = APITools.send(ARTICLE_SERVER + articleId + "/article");
		if (article.get(0).getAvailable() < 1) throw new APITools.DemoException("Not enough items on stock");
		Order order = orderRepository.save(new Order(articleId, amount));
		APITools.send(ARTICLE_SERVER + order.getCode() + "/" + articleId + "/" + amount + "/reserve", () -> deleteOrder(order));
		return order;
	}

	public List<Order> clear() throws APITools.DemoException {
		orderRepository.deleteAll();
		APITools.send(ARTICLE_SERVER  + "restore");
		return orders();
	}

	public Order buy(String orderId) throws APITools.DemoException {
		Order order = orderRepository.findFirstByCode(orderId);
		if (order == null) throw new APITools.DemoException("Order not found");
		if (order.getStatus().equals(OrderStatus.BOUGHT)) throw new APITools.DemoException("Order already completed");
		APITools.send(ARTICLE_SERVER + orderId + "/buy");
		order.setStatus(OrderStatus.BOUGHT);
		return orderRepository.save(order);
	}

	public List<ArticleDTO> articles() throws APITools.DemoException {
		return APITools.send(ARTICLE_SERVER);
	}

	private void deleteOrder(Order order) {
		this.orderRepository.delete(order);
	}

}
