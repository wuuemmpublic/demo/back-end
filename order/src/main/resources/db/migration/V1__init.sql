-- noinspection SqlNoDataSourceInspectionForFile
-- language H2

create table if not exists orders
(
    id            bigint not null primary key auto_increment,
    article_id    varchar_ignorecase,
    amount        int,
    code          varchar_ignorecase,
    status        varchar_ignorecase
);
