#!/bin/bash

docker stop demo-article
docker stop demo-order
docker stop demo-front
docker network rm demo-net
docker rm -f demo-article
docker rm -f demo-order
docker rm -f demo-front
