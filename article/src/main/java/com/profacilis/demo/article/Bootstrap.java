package com.profacilis.demo.article;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap implements CommandLineRunner {

	private final ArticleService articleService;

	public Bootstrap(ArticleService articleService) {
		this.articleService = articleService;
	}

	@Override
	public void run(String... args) throws Exception {
		if (articleService.getAll().isEmpty()) articleService.clear();
	}
}
