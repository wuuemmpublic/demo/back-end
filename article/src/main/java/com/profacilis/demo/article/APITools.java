package com.profacilis.demo.article;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
class APITools {

	private final static RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();

	interface ServiceThrowable<T> {
		T get() throws Exception;
	}

	interface ServiceVoidThrowable {
		void get() throws Exception;
	}

	static class DemoException extends Exception {
		public DemoException(String message) {
			super(message);
		}
	}

	static <T> T response(ServiceThrowable<T> action) {
		try {
			return action.get();
		} catch (DemoException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	static void responseVoid(ServiceVoidThrowable action) {
		try {
			action.get();
		} catch (DemoException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	private static MultiValueMap<String, String> buildHeaders() {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		return headers;
	}

	private static RestTemplate buildTemplate() {
		return restTemplateBuilder.build();
	}

}
