package com.profacilis.demo.article;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Reservation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private int amount;
	private String orderId;

	public Reservation(int amount, String orderId, Article article) {
		this.amount = amount;
		this.orderId = orderId;
		this.article = article;
	}

	@ManyToOne
	@JoinColumn(name = "article", referencedColumnName = "id")
	private Article article;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Reservation that = (Reservation) o;
		return amount == that.amount && Objects.equals(id, that.id) && Objects.equals(orderId, that.orderId) && Objects.equals(article, that.article);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, amount, orderId, article);
	}
}
