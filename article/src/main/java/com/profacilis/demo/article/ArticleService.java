package com.profacilis.demo.article;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Service layer of Article microservice
 */

@Service
public class ArticleService {

	private final ArticleRepository articleRepository;
	private final ReservationRepository reservationRepository;

	public ArticleService(ArticleRepository articleRepository, ReservationRepository reservationRepository) {
		this.articleRepository = articleRepository;
		this.reservationRepository = reservationRepository;
	}

	public List<ArticleDTO> getAll() {
		ArrayList<ArticleDTO> result = new ArrayList<>();
		for (Article article: articleRepository.findAll()) {
			result.add(transform(article));
		}
		return result;
	}

	public List<ArticleDTO> clear() {
		reservationRepository.deleteAll();
		articleRepository.deleteAll();
		articleRepository.save(new Article("alpha", 5, "A"));
		articleRepository.save(new Article("beta", 5, "B"));
		articleRepository.save(new Article("gamma", 5, "C"));

		return getAll();
	}

	public List<ArticleDTO> get(String articleId) throws APITools.DemoException {
		return Collections.singletonList(transform(getArticle(articleId)));
	}

	public List<ArticleDTO> reserve(String orderId, String articleId, int amount) throws APITools.DemoException {
		Article article = getArticle(articleId);
		if (available(articleId) < amount) throw new APITools.DemoException("Not enough items in stock");
		reservationRepository.save(new Reservation(amount, orderId, article));
		return Collections.singletonList(transform(article));
	}

	public List<ArticleDTO> buy(String orderId) throws APITools.DemoException {
		Reservation reservation = reservationRepository.findFirstByOrderId(orderId);
		if (reservation == null) throw new APITools.DemoException("No such a reservation");
		Article article = reservation.getArticle();
		article.setStock(article.getStock() - reservation.getAmount());
		reservationRepository.delete(reservation);
		return Collections.singletonList(transform(articleRepository.save(article)));
	}

	private Article getArticle(String articleId) throws APITools.DemoException {
		Article article = articleRepository.findFirstByCode(articleId);
		if (article == null) throw new APITools.DemoException("Article not found");
		return article;
	}

	private int available(String articleId) throws APITools.DemoException {
		Article article = getArticle(articleId);
		return article.getStock() - reservedAmount(article);
	}

	private int reservedAmount(Article article) {
		int total = 0;
		for (Reservation reservation: reservationRepository.findAllByArticle(article)) {
			total += reservation.getAmount();
		}
		return total;
	}

	private ArticleDTO transform(Article article) {
		ArticleDTO dto = new ArticleDTO();
		BeanUtils.copyProperties(article, dto);
		dto.setAvailable(article.getStock() - reservedAmount(article));
		return dto;
	}

}
