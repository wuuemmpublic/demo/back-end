package com.profacilis.demo.article;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface ReservationRepository extends JpaRepository<Reservation, Long> {

	List<Reservation> findAllByArticle(Article article);

	long countAllByArticleAndOrderId(Article article, String orderId);

	void deleteAllByArticleAndOrderId(Article article, String orderId);

	Reservation findFirstByOrderId(String orderId);

}
