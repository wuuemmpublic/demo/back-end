package com.profacilis.demo.article;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.profacilis.demo.article.APITools.*;

/**
 * API accessible only via authorized call from another service (Order) due to interception applied
 * Not available for front-end calls
 */

@RestController
@Slf4j
public class ArticleController {

	private final ArticleService articleService;

	public ArticleController(ArticleService articleService) {
		this.articleService = articleService;
	}

	/**
	 * restores data to initial state (three articles, each 5 items in stock, no reservations)
	 * @return list of articles
	 */
	@GetMapping("/restore")
	public List<ArticleDTO> clear() {
		return articleService.clear();
	}

	/**
	 *
	 * @param code article's code
	 * @return one-element list of Articles
	 */
	@GetMapping("/{id}/article")
	public List<ArticleDTO> getArticle(@PathVariable("id") String code) {
		return response(() -> articleService.get(code));
	}

	/**
	 *
	 * @return list of all articles
	 */
	@GetMapping("/")
	public List<ArticleDTO> articles() {
		return articleService.getAll();
	}

	/**
	 *
	 * @param orderId orders's code
	 * @param articleId article's code
	 * @param amount amount reserved
	 * @return one-element list of Articles (the one reserved)
	 */
	@GetMapping("/{order}/{article}/{amount}/reserve")
	public List<ArticleDTO> reserve(@PathVariable("order") String orderId, @PathVariable("article") String articleId, @PathVariable("amount") int amount) {
		return response(() -> articleService.reserve(orderId, articleId, amount));
	}

	/**
	 *
	 * @param orderId order's code
	 * @return one-element list of Articles (the one bought)
	 */
	@GetMapping("/{order}/buy")
	public List<ArticleDTO> buy(@PathVariable("order") String orderId) {
		return response(() -> articleService.buy(orderId));
	}

}
