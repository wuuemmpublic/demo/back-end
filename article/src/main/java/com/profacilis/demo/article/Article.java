package com.profacilis.demo.article;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Article {

	@Id()
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	String name;
	int stock;
	String code;

	public Article(String name, int stock, String code) {
		this.name = name;
		this.stock = stock;
		this.code = code;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Article article = (Article) o;
		return stock == article.stock && Objects.equals(id, article.id) && Objects.equals(name, article.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, stock);
	}
}
