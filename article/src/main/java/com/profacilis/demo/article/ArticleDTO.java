package com.profacilis.demo.article;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
class ArticleDTO extends Article {

	@JsonIgnore
	Long id;
	private int available;

}
