package com.profacilis.demo.article;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@SpringBootApplication
public class ArticleApplication {

	private static final String USER = "demo";
	private static final String PASS = "secret";

	public static void main(String[] args) {
		SpringApplication.run(ArticleApplication.class, args);
	}

	@Bean
	HandlerInterceptor articleInterceptor() {
		return new HandlerInterceptor() {

			@Override
			public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
				String auth = request.getHeader("Authorization");
				if (auth != null) {
					auth = auth.replace("Basic","").replace(" ","");
					String decoded = new String(Base64.getDecoder().decode(auth), StandardCharsets.UTF_8);
					String[] credentials = StringUtils.split(decoded,':');
					if (credentials.length == 2) {
						if (credentials[0].equals(USER) && credentials[1].equals(PASS)) {
							return true;
						}
					}
				}
				response.sendError(HttpServletResponse.SC_FORBIDDEN, "Access denied");
				return true;
			}
		};
	}

}
