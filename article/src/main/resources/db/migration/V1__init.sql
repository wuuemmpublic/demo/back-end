-- noinspection SqlNoDataSourceInspectionForFile
-- language H2

create table if not exists article (
    id bigint not null primary key auto_increment,
    name varchar_ignorecase,
    stock int,
    code varchar_ignorecase unique
);

create table if not exists reservation
(
    id         bigint not null primary key auto_increment,
    article    bigint,
    amount     int,
    order_id   varchar_ignorecase
);

alter table reservation add constraint fk_article_in_reservation foreign key (article) references article(id);
