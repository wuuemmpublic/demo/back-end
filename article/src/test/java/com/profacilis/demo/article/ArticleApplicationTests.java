package com.profacilis.demo.article;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * samples only
 * unit testing is disabled in build phase
 */

@SpringBootTest
class ArticleApplicationTests {

	private final ArticleService articleService;

	@Autowired
	public ArticleApplicationTests(ArticleService articleService) {
		this.articleService = articleService;
	}

	@BeforeEach
	void clear() {
		articleService.clear();
	}

	@Test
	void notFound() {
		assertThrows(APITools.DemoException.class, () -> articleService.get("D"));
	}

	@Test
	void found() {
		HashMap<String, String> map = new HashMap<>();
		map.put("A", "alpha");
		map.put("B", "beta");
		map.put("C", "gamma");
		for (Map.Entry<String, String> entry: map.entrySet()) {
			assertDoesNotThrow(() -> assertEquals(entry.getValue(), articleService.get(entry.getKey()).get(0).getName()));
		}
	}



}
